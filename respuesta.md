## Ejercicio 3
*¿Cuántos paquetes componen la captura?

#### Respuesta: 1050 paquetes.

*¿Cuánto tiempo dura la captura?

#### Respuesta: Dura 10'52 segundos.

*¿Qué IP tiene la máquina donde se ha efectuado la captura?

#### Respuesta: La ip es 192.168.1.116

*¿Se trata de una IP pública o de una IP privada?

#### Respuesta: Trata de una IP privada.

*¿Por qué lo sabes?

#### Respuesta: Pertenece al rango de redes privadas.

*¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?

#### Respuesta: UDP, SIP y RTP.

*¿Qué otros protocolos podemos ver en la jerarquía de protocolos?

#### Respuesta: Ethernet, IP e ICMP.

*¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?

#### Respuesta: RTP, el tráfico es de 134 Kbits por segundo.

*¿En qué segundos tienen lugar los dos primeros envíos SIP?

#### Respuesta: En el segundo 0 los dos.

*Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?

#### Respuesta: Desde el paquete nº 6.

*Los paquetes RTP, ¿cada cuánto se envían?

#### Respuesta: Cada 0.01 segundos.



## Ejercicio 4
*¿De qué protocolo de nivel de aplicación son?

#### Respuesta: Protocolo SIP.

*¿Cuál es la dirección IP de la máquina "Linphone"?

#### Respuesta: 192.168.1.116

*¿Cuál es la dirección IP de la máquina "Servidor"?

#### Respuesta: 212.79.111.155

*¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?

#### Respuesta: Linphone envia un Request para mantener conexión con el servidor.

*¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?

#### Respuesta: Se envía un trying desde el servidor a LinPhone.

*¿De qué protocolo de nivel de aplicación son?

#### Respuesta: Protocolo SIP.

*¿Entre qué máquinas se envía cada trama?

#### Respuesta: Linphone-Servidor.

*¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?

#### Respuesta: El servidor envia OK.

*¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?

#### Respuesta: Linphone envia una trama ACK.



## Ejercicio 5
*¿Qué número de trama es?

#### Respuesta: 1042.

*¿De qué máquina a qué máquina va?

#### Respuesta: Linphone-Servidor.

*¿Para qué sirve?

#### Respuesta: Acabar la comunicación con el servidor.

*¿Puedes localizar en ella qué versión de Linphone se está usando?

#### Respuesta: Linphone Desktop/ 4.3.2



## Ejercicio 6
*¿Cuál es la direccion SIP con la que se quiere establecer una llamada?

#### Respuesta: music@sip.iptel.org

*¿Qué instrucciones SIP entiende el UA?

#### Respuesta: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER,  NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE.

*¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?

#### Respuesta: content-type.

*¿Cuál es el nombre de la sesión SIP?

#### Respuesta: Talk.



## Ejercicio 7
*¿Qué trama lleva esta propuesta?

#### Respuesta: La 2ª.

*¿Qué indica el 7078?

#### Respuesta: Dirección de transporte.

*¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?

#### Respuesta: Puerto que envia/recibe esos paquetes.

*¿Qué paquetes son esos?

#### Respuesta: RTP.

*¿Qué trama lleva esta respuesta?

#### Respuesta: La 4ª.

*¿Qué valor es el XXX?

#### Respuesta: 29448.

*¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?

#### Respuesta: Puerto que envia/recibe esos paquetes.

*¿Qué paquetes son esos?

#### Respuesta: RTP.



## Ejercicio 8
*¿De qué máquina a qué máquina va?

#### Respuesta: Linphone- Servidor.

*¿Qué tipo de datos transporta?

#### Respuesta: Multimedia.

*¿Qué tamaño tiene?

#### Respuesta: 214 bytes.

*¿Cuántos bits van en la "carga de pago" (payload)?

#### Respuesta: 1280 bits.

*¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?

#### Respuesta: 0,01 segundos.



## Ejercicio 9
*¿Cuántos flujos hay? ¿por qué?

#### Respuesta: Dos, porque hay en ambas direcciones (cliente-servidor/servidor-cliente).

*¿Cuántos paquetes se pierden?

#### Respuesta: No se pierden paquetes.

*Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?

#### Respuesta: 30,73 ms.

*¿Qué es lo que significa el valor de delta?

#### Respuesta: Espacio temporal entre envio de paquetes.

*¿En qué flujo son mayores los valores de jitter (medio y máximo)?

#### Respuesta: Servidor-LinPhone.

*¿Qué significan esos valores?

#### Respuesta: La variación entre los retardos.

*¿Cuánto valen el delta y el jitter para ese paquete?

#### Respuesta: Delta: 0,00016 y jitter: 3,09.

*¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?

#### Respuesta: Si, fijandonos en el skew.

*El "skew" es negativo, ¿qué quiere decir eso?

#### Respuesta: Significa que el paquete está llegando con antelación.

*¿Qué se oye al pulsar play?

#### Respuesta: Una pista de audio.

*¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?

#### Respuesta: Se escucha el audio cortado.

*¿A qué se debe la diferencia?

#### Respuesta: Se debe a la poca espera de paquetes (1ms).




## Ejercicio 10
*¿Cuánto dura la llamada?

#### Respuesta: 10 segundos.

*¿En qué segundo se recibe el último OK que marca el final de la llamada?

#### Respuesta: En el segundo 10,52.

*¿Cuáles son las SSRC que intervienen?

#### Respuesta: Linphone-Server: 0x0d2db8b4  Server-Linphone: 0x5c44a34b.

*¿Cuántos paquetes se envían desde LinPhone hasta Servidor?

#### Respuesta: 514 .

*¿Cuál es la frecuencia de muestreo del audio?

#### Respuesta: 8 KHz.

*¿Qué formato se usa para los paquetes de audio (payload)?

#### Respuesta: g711U.
